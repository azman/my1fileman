# makefile for my1fileman

EXETOOL = my1fileman
OBJTOOL = my1list.o my1path.o my1text.o my1conf.o $(EXETOOL).o

EXTPATH = ../my1codelib/src

CFLAG += -Wall -I$(EXTPATH)
CFLAG += -DMY1APPVERS=\"$(shell date +%Y%m%d)\"
TFLAG := $(LFLAG) # expand current LFLAG
LFLAG = $(TFLAG) -lncurses

RM = rm -f
CC = gcc -c
LD = gcc

debug: CFLAG += -g -DMY1_DEBUG
static: SFLAG = -static
static: LFLAG = $(TFLAG) libncurses.a

all: $(EXETOOL)

new: clean all

debug: new

static: new
	strip ${EXETOOL}

${EXETOOL}: $(OBJTOOL)
	$(LD) $(SFLAG) $+ -o $@ $(LFLAG)

%.o: src/%.c src/%.h
	$(CC) $(CFLAG) -o $@ $<

%.o: src/%.c
	$(CC) $(CFLAG) -o $@ $<

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAG) -o $@ $<

clean:
	-$(RM) $(EXETOOL) $(OBJTOOL) *.o
