/*----------------------------------------------------------------------------*/
#include <curses.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>
#include <ctype.h>
/*----------------------------------------------------------------------------*/
#define STR_TOKEN_UTIL
#include "my1path.h"
#include "my1conf.h"
/*----------------------------------------------------------------------------*/
#ifndef MY1APPVERS
#define MY1APPVERS "build"
#endif
/*----------------------------------------------------------------------------*/
#define CONFIG_FILE "my1fileman.cfg"
#define DEFAULT_TERMINAL "st"
#define DEFAULT_EDITOR "vi"
#define MESG_EXPIRE 3
#define TOKEN_COUNT 7 
#define PARAM_COUNT (TOKEN_COUNT+1)
/*----------------------------------------------------------------------------*/
int find_in_path(char* exec)
{
	int flag = 0;
	char *pick;
	my1list_t list;
	my1path_t that;
	list_init(&list,LIST_TYPE_DEFAULT);
	if (path_getenv(&list))
	{
		list.curr = 0x0;
		while (!flag&&list_iterate(&list))
		{
			pick = path_name_merge((char*)list.curr->item,exec);
			path_init(&that);
			path_dostat(&that,pick);
			/* check if file is found... and executable (by everybody) */
			if ((that.flag&PATH_FLAG_FILE)&&(that.perm[9]=='x'))
				flag = 1;
			path_free(&that);
			free((void*)pick);
		}
	}
	list_free(&list,list_free_item);
	return flag;
}
/*----------------------------------------------------------------------------*/
#define STAT_SIZE 128
/*----------------------------------------------------------------------------*/
typedef struct _pathview_t
{
	int rows, cols, vrow, crow, lrow;
	WINDOW *mwin, *hwin, *fwin;
	my1path_t path;
	my1list_t list;
	my1str_t term_name, term_opts, stat_mesg, stat_main;
	my1str_t exec_name, exec_opts, text_edit;
	my1str_t init_path, mark_path;
	char *term, *opts, *init, *home;
	char stat[STAT_SIZE];
	int skip, size;
}
pathview_t;
/*----------------------------------------------------------------------------*/
/** global pointer for alarm_handler */
pathview_t* pview = 0x0;
/*----------------------------------------------------------------------------*/
void pathview_init(pathview_t* show)
{
	int rows, cols, vrow;
	/* initialize curses */
	initscr();
	cbreak(); /* disable buffer / get 1-char-at-a-time */
	noecho(); /* disable local echo */
	curs_set(0);
	rows = getmaxy(stdscr);
	cols = getmaxx(stdscr);
	vrow = rows - 4;
	/* save rows/cols info */
	show->cols = cols;
	show->rows = rows;
	show->vrow = vrow;
	show->crow = 0;
	show->lrow = 0;
	/* top - header window to show path name */
	show->hwin = newwin(2,cols,0,0);
	/* mid - main interface window */
	show->mwin = newwin(vrow,cols,2,0);
	nodelay(show->mwin,TRUE); /* non-blocking, returns ERR when no key */
	keypad(show->mwin,TRUE); /* get special keys (escaped keys?) */
	/* bot - footer window to show info */
	show->fwin = newwin(2,cols,rows-2,0);
	/* get current path */
	path_init(&show->path);
	path_getcwd(&show->path);
	list_init(&show->list,LIST_TYPE_DEFAULT);
	/* config stuffs */
	str_init(&show->term_name);
	str_init(&show->term_opts);
	str_init(&show->stat_mesg);
	str_init(&show->stat_main);
	str_init(&show->exec_name);
	str_init(&show->exec_opts);
	str_init(&show->text_edit);
	str_init(&show->init_path);
	str_init(&show->mark_path);
	/* terminal program */
	show->term = 0x0;
	show->opts = 0x0;
	/* initial path and HOME path */
	str_assign(&show->init_path,show->path.full);
	show->init = show->init_path.buff;
	show->home = getenv("HOME");
	/* status message */
	show->stat[0] = 0x0;
	/* display param */
	show->skip = 0;
	show->size = 0;
}
/*----------------------------------------------------------------------------*/
void pathview_free(pathview_t* show)
{
	str_free(&show->term_name);
	str_free(&show->term_opts);
	str_free(&show->stat_mesg);
	str_free(&show->stat_main);
	str_free(&show->exec_name);
	str_free(&show->exec_opts);
	str_free(&show->text_edit);
	str_free(&show->init_path);
	str_free(&show->mark_path);
	delwin(show->hwin);
	delwin(show->mwin);
	delwin(show->fwin);
	path_free(&show->path);
	list_free(&show->list,list_free_item);
	endwin();
}
/*----------------------------------------------------------------------------*/
void pathview_config(pathview_t* show, char *arg0, char *conf)
{
	int test;
	my1str_t that;
	char prefix[] = { PATH_SEPARATOR_CHAR, 0x0 };
	str_init(&that);
	do
	{
		/* look for config in pwd and home  */
		str_assign(&that,show->path.full);
		str_append(&that,prefix);
		str_append(&that,conf);
		if ((test=conf_validate(that.buff))!=MY1CONF_VALID)
		{
			str_assign(&that,show->home);
			str_append(&that,prefix);
			str_append(&that,".config");
			str_append(&that,prefix);
			str_append(&that,conf);
			if ((test=conf_validate(that.buff))!=MY1CONF_VALID)
				break;
		}
		conf = that.buff;
		/* terminal */
		test = conf_find_param(conf,"Terminal","Exec",&show->term_name);
		if (test==MY1CONF_VALID) show->term = show->term_name.buff;
		if (show->term)
		{
			test = conf_find_param(conf,"Terminal","Args",&show->term_opts);
			if (test==MY1CONF_VALID) show->opts = show->term_opts.buff;
		}
		/* text editor */
		test = conf_find_param(conf,"TextEdit","Exec",&show->text_edit);
		if (test!=MY1CONF_VALID||!find_in_path(show->text_edit.buff))
			str_null(&show->text_edit);
	}
	while (0);
	str_free(&that);
	/* use defaults if still not assigned */
	if (!show->term&&find_in_path(DEFAULT_TERMINAL))
	{
		str_assign(&show->term_name,DEFAULT_TERMINAL);
		show->term = show->term_name.buff;
	}
	if (!show->text_edit.fill&&find_in_path(DEFAULT_EDITOR))
		str_assign(&show->text_edit,DEFAULT_EDITOR);
}
/*----------------------------------------------------------------------------*/
void pathview_info_raw(pathview_t* show, char* info)
{
	wmove(show->fwin,1,0);
	wprintw(show->fwin,info);
	wclrtoeol(show->fwin);
	wrefresh(show->fwin);
}
/*----------------------------------------------------------------------------*/
void pathview_info(pathview_t* show, char* info)
{
	char stat[STAT_SIZE];
	snprintf(stat,STAT_SIZE,"[INFO] %s",info);
	pathview_info_raw(show,stat);
	alarm(MESG_EXPIRE);
}
/*----------------------------------------------------------------------------*/
void alarm_handler(int signum)
{
	(void) signum;
	if (!pview) return;
	pathview_info_raw(pview,pview->stat);
}
/*----------------------------------------------------------------------------*/
void pathview_list(pathview_t* show)
{
	my1list_t buff;
	my1path_t temp;
	my1str_t pack;
	int mark, step;
	char *pick, psep[] = { PATH_SEPARATOR_CHAR, '\0' };
	/* show path info */
	wmove(show->hwin,0,0);
	wprintw(show->hwin,"[PATH] ");
	wprintw(show->hwin,show->path.full);
	wclrtobot(show->hwin);
	wrefresh(show->hwin);
	/* initialize list pathview */
	if (show->list.size)
	{
		/* clean existing list */
		list_free(&show->list,list_free_item);
		list_init(&show->list,LIST_TYPE_DEFAULT);
	}
	/* get the list in buffer */
	list_init(&buff,LIST_TYPE_DEFAULT);
	path_dolist(&show->path,&buff,PATH_LIST_BOTH);
	buff.curr = 0x0; mark = 0; step = 0;
	while (list_iterate(&buff))
	{
		pick = (char*) buff.curr->item;
		path_init(&temp);
		path_access(&temp,pick);
		if (temp.name)
		{
			str_init(&pack); /* special case, pack.buff is kept in list! */
			str_assign(&pack,temp.perm);
			str_append(&pack," ");
			str_append(&pack,temp.name);
			if (temp.real)
			{
				str_append(&pack," -> ");
				str_append(&pack,temp.real);
			}
			if (temp.flag&PATH_FLAG_PATH)
				str_append(&pack,psep);
			/* insert into list */
			list_push_item(&show->list,(void*)pack.buff);
			/* check mark request */
			if (show->mark_path.fill>0) {
				if (!strncmp(show->mark_path.buff,temp.name,
						show->mark_path.fill+1)) {
					mark = step;
				}
			}
		}
		path_free(&temp);
		step++;
	}
	/* reset index */
	show->size = show->list.size;
	if (show->size<show->vrow) show->lrow = show->size-1;
	else show->lrow = show->vrow-1;
	if (mark>0) {
		show->crow = mark % show->vrow;
		show->skip = mark - show->crow;
	} else {
		show->skip = 0;
		show->crow = 0;
	}
	/* cleanup */
	list_free(&buff,list_free_item);
}
/*----------------------------------------------------------------------------*/
void pathview_update(pathview_t* show)
{
	char buff, *pick = 0x0, *temp;
	int loop = 0, step = show->skip;
	my1list_t *list = &show->list;
	if (!show->skip) list->curr = 0x0;
	else list->curr = list_select(list,(show->skip-1));
	wmove(show->mwin,0,0);
	while (list_iterate(list))
	{
		temp = (char*) list->curr->item;
		if (loop==show->crow)
		{
			buff = '>';
			pick = temp;
			step += loop;
		}
		else buff = ' ';
		wprintw(show->mwin,"%c %s\n",buff,temp);
		loop++;
		if (loop>show->lrow)
			break;
	}
	wclrtobot(show->mwin);
	wrefresh(show->mwin);
	/* prepare footer message buffer */
	if (pick)
	{
		while ((*pick)&&(*pick)!=' ') pick++;
		if (*pick)
		{
			pick++;
			snprintf(show->stat,STAT_SIZE,"[%d/%d] {%s}",
				(step+1),show->size,pick);
		}
	}
	else
		snprintf(show->stat,STAT_SIZE,"[EMPTY-(%d,%d)]",show->crow,show->lrow);
	/* update footer */
	wmove(show->fwin,1,0);
	wprintw(show->fwin,show->stat);
	wclrtoeol(show->fwin);
	wrefresh(show->fwin);
}
/*----------------------------------------------------------------------------*/
int pathview_resize(pathview_t* show)
{
	/* update rows/cols info */
	show->cols = getmaxx(stdscr);
	show->rows = getmaxy(stdscr);
	show->vrow = show->rows - 4;
	if (show->crow>=show->vrow)
		show->crow = show->vrow-1;
	show->lrow = show->vrow-1;
	/* resize all */
	wresize(show->hwin,2,show->cols);
	wresize(show->mwin,show->vrow,show->cols);
	wresize(show->fwin,2,show->cols);
	mvwin(show->fwin,show->rows-2,0);
	/* redraw */
	return 1;
}
/*----------------------------------------------------------------------------*/
int pathview_next(pathview_t* show)
{
	int flag = 0;
	if ((show->skip+show->crow)<show->size-1)
	{
		if (show->crow<show->lrow) show->crow++;
		else show->skip++;
		flag = 1;
	}
	return flag;
}
/*----------------------------------------------------------------------------*/
int pathview_next_page(pathview_t* show)
{
	int temp, flag = 0;
	if (show->size>show->vrow)
	{
		/* more items off view */
		temp = show->skip + show->vrow; // skip page!
		/* are we still within boundary? */
		if (temp<show->size-1)
		{
			show->skip = temp;
			if (temp+show->crow>=show->size)
				show->crow = 0;
			flag = 1;
		}

	}
	return flag;
}
/*----------------------------------------------------------------------------*/
int pathview_prev(pathview_t* show)
{
	int flag = 0;
	if (show->crow>0)
	{
		show->crow--;
		flag = 1;
	}
	else
	{
		if (show->skip>0)
		{
			show->skip--;
			flag = 1;
		}
		show->crow = 0; /* just in case? */
	}
	return flag;
}
/*----------------------------------------------------------------------------*/
int pathview_prev_page(pathview_t* show)
{
	int flag = 0;
	if (show->skip>0)
	{
		if (show->skip>=show->vrow)
			show->skip -= show->vrow;
		else
			show->skip = 0;
		flag = 1;
	}
	return flag;
}
/*----------------------------------------------------------------------------*/
#define LISTNAME_OFFSET 11
/*----------------------------------------------------------------------------*/
int pathview_doexec(pathview_t* show)
{
	my1str_token_t find;
	my1str_t buff;
	pid_t ppid;
	char* args[PARAM_COUNT];
	int step;
	str_init(&buff);
	do
	{
		if (!show->term)
		{
			pathview_info(show,"** No terminal defined!");
			break;
		}
		if (!find_in_path(show->term))
		{
			str_assign(&buff,"** Cannot find terminal '");
			str_append(&buff,show->term);
			str_append(&buff,"'!");
			pathview_info(show,buff.buff);
			break;
		}
		ppid = fork();
		if (ppid<0) pathview_info(show,"** Cannot fork to run executable!");
		else if (ppid>0)
		{
			if (waitpid(ppid,0x0,0)<0)
				pathview_info(show,"** Something went wrong?");
			else
			{
				str_assign(&buff,"-- Term{");
				str_append(&buff,show->term);
				if (show->opts)
				{
					str_append(&buff,":");
					str_append(&buff,show->opts);
				}
				str_append(&buff,"}");
				if (show->exec_name.fill>0)
				{
					str_append(&buff," && Exec{");
					str_append(&buff,show->exec_name.buff);
					if (show->exec_opts.fill)
					{
						str_append(&buff,":");
						str_append(&buff,show->exec_opts.buff);
					}
					str_append(&buff,"}");
				}
				pathview_info(show,buff.buff);
			}
		}
		else
		{
			str_free(&buff); /* not sure if this is needed? */
			close(STDERR_FILENO); /* do not use parent stderr! */
			/* fork again so this parent can terminate (prevent zombies!) */
			ppid = fork();
			if (!ppid)
			{
				/* create new session - detach from parent process */
				ppid = setsid(); /** group id? new session! */
				str_init(&buff);
				step = 0;
				args[step++] = show->term;
				if (show->opts)
				{
					str_assign(&buff,show->opts);
					strtok_init(&find,COMMON_DELIM);
					strtok_prep(&find,&buff);
					while (strtok_next(&find)&&step<TOKEN_COUNT)
						args[step++] = strdup(find.token.buff);
					strtok_free(&find);
				}
				if (show->exec_name.fill>0)
				{
					args[step++] = show->exec_name.buff;
					if (show->exec_opts.fill)
					{
						str_assign(&buff,show->exec_opts.buff);
						strtok_init(&find,COMMON_DELIM);
						strtok_prep(&find,&buff);
						while (strtok_next(&find)&&step<TOKEN_COUNT)
							args[step++] = strdup(find.token.buff);
						strtok_free(&find);
					}
				}
				args[step] = 0x0;
				str_free(&buff);
				/* change dir */
				chdir(show->path.full);
				/* child process execute terminal */
				execvp(args[0],args);
				exit(-1);
			}
			/* parent gracefully exit! */
			exit(ppid<0?-1:0);
		}
	}
	while (0);
	str_free(&buff);
	return 0;
}
/*----------------------------------------------------------------------------*/
/* check first 10kB */
#define TEXTCHAR_MAX_ITERATION 102400
#define TEXTCHAR_THRESHOLD_PCT 80
/*----------------------------------------------------------------------------*/
int is_text(char* filename)
{
	FILE* read;
	int flag, test, step, asci;
	flag = 0;
	read = fopen(filename,"r");
	if (read)
	{
		step = 0; asci = 0;
		while ((test=fgetc(read))!=EOF)
		{
			if (isprint(test)) /* ascii range? */
				asci++;
			step++;
			if (step==TEXTCHAR_MAX_ITERATION) break;
		}
		fclose(read);
		test = 0;
		if (step>0) test = asci*100/step;
		if (test>=TEXTCHAR_THRESHOLD_PCT) flag = 1;
	}
	return flag;
}
/*----------------------------------------------------------------------------*/
int pathview_dopick(pathview_t* show)
{
	char *pick, *full;
	int flag = 0, test = show->skip + show->crow;
	my1path_t *path = &show->path;
	my1list_t *list = &show->list;
	my1item_t *item = list_select(list,test);
	pick = 0x0;
	if (item)
	{
		pick = (char*) item->item;
		test = strlen(pick);
		if (pick[test-1]==PATH_SEPARATOR_CHAR)
		{
			/* a path -> chdir!? */
			if (pick[0]=='l') /* it is a link! */
			{
				int loop = LISTNAME_OFFSET;
				while (pick[loop]!=' ') loop++;
				pick[loop] = 0x0;
			}
			full = path_name_merge(path->full,&pick[LISTNAME_OFFSET]);
			path_chdir(path,full);
			pathview_list(show);
			flag = 1;
			free((void*)full);
		}
		else
		{
			full = path_name_merge(path->full,&pick[LISTNAME_OFFSET]);
			if (is_text(full))
			{
				if (show->text_edit.fill)
				{
					str_assign(&show->exec_name,show->text_edit.buff);
					str_assign(&show->exec_opts,full);
				}
				else pathview_info(show,"@@ No handler for text!");
			}
			else
				pathview_info(show,"@@ No handler for this file!");
			free((void*)full);
			if (show->exec_name.fill>0)
			{
				pathview_doexec(show);
				str_null(&show->exec_name);
				str_null(&show->exec_opts);
			}
		}
	}
	return flag;
}
/*----------------------------------------------------------------------------*/
int pathview_parent(pathview_t* show)
{
	int flag = 0;
	char* parent = strdup(show->path.path);
	if (strcmp(parent,show->path.full))
	{
		str_setstr(&show->mark_path,show->path.name);
		path_dostat(&show->path,parent);
		pathview_list(show);
		str_null(&show->mark_path);
		/* change dir? */
		flag = 1;
	}
	free((void*)parent);
	return flag;
}
/*----------------------------------------------------------------------------*/
int pathview_doinit(pathview_t* show)
{
	int flag = 0;
	if (show->init)
	{
		path_dostat(&show->path,show->init);
		pathview_list(show);
		flag = 1;
	}
	return flag;
}
/*----------------------------------------------------------------------------*/
int pathview_gohome(pathview_t* show)
{
	int flag = 0;
	if (show->home)
	{
		path_dostat(&show->path,show->home);
		pathview_list(show);
		flag = 1;
	}
	return flag;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	pathview_t pathview;
	struct sigaction action;
	int loop, test;
	int fail = 0, done = 0, flag = 0;
	char *conf = CONFIG_FILE;
	/* check for terminal options */
	for (loop=1;loop<argc;loop++)
	{
		if (!strncmp(argv[loop],"--config",8))
		{
			loop++;
			if (loop<argc) conf = argv[loop];
			else
			{
				printf("-- [ERROR] No config given!\n");
				fail = 2;
			}
		}
		else
		{
			printf("-- [ERROR] Unknown option '%s'!\n", argv[loop]);
			fail = 1;
		}
	}
	if (fail) return 1;
	/* setup signal handler for alarm() */
	memset(&action,0,sizeof(action));
	action.sa_handler = &alarm_handler;
	test = sigaction(SIGALRM, &action,0x0);
	if (test)
	{
		printf("[ERROR] sigaction failed! (%d)\n",test);
		exit(-1);
	}
	/* alarm handler needs this! */
	pview = &pathview;
	/* init pathview */
	pathview_init(&pathview);
	pathview_config(&pathview,argv[0],conf);
	/* show current path */
	pathview_list(&pathview);
	pathview_update(&pathview);
	pathview_info(&pathview,"@@ Version:" MY1APPVERS);
	/* main loop */
	while(!done)
	{
		/* check if we need to update */
		if (flag)
		{
			pathview_update(&pathview);
			flag = 0;
		}
		test = wgetch(pathview.mwin);
		switch (test)
		{
			case 'q': done = 1; break;
			case 'r': pathview_list(&pathview); flag = 1; break;
			case 'h': case '~': flag = pathview_gohome(&pathview); break;
			case 't': flag = pathview_doexec(&pathview); break;
			case 'i': flag = pathview_doinit(&pathview); break;
			case KEY_UP: flag = pathview_prev(&pathview); break;
			case KEY_DOWN: flag = pathview_next(&pathview); break;
			case KEY_PPAGE: flag = pathview_prev_page(&pathview); break;
			case KEY_NPAGE: flag = pathview_next_page(&pathview); break;
			case KEY_LEFT: flag = pathview_parent(&pathview); break;
			case KEY_RIGHT: flag = pathview_dopick(&pathview); break;
			case KEY_RESIZE: flag = pathview_resize(&pathview); break;
		}
	}
	/* free pathview */
	pathview_free(&pathview);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
